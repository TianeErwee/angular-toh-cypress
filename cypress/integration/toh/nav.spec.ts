/// <reference types="Cypress" />

describe('nav', () => {
  it('should open dashboard', () => {
    cy.visit('http://localhost:4200');
    cy.wait(1000);
    cy.url().should('include', '/dashboard');
  });

  it('should search for hero', () => {
    cy.get('#search-box').type('Dr');
    cy.wait(1000);
    cy.get('#found-heroes').children().should('have.length', 2);
  });

  it('should search for hero', () => {
    cy.get('#search-box').clear();
    cy.wait(1000);
    cy.get('#found-heroes').children().should('have.length', 0);
  });

  it('should navigate to heroes list', () => {
    cy.get('#heroes').click();
    cy.url().should('include', 'heroes');
  });

  it('should nav to hero 11', () => {
    cy.get('#11').click();
    cy.url().should('include', 'detail/11');
  });

  it('should change name to test name', () => {
    cy.get('#name-input').type('test name{enter}');
    cy.get('#hero-name').should('contain.text', 'TEST NAME Details');
    cy.get('#save-button').click();
    cy.wait(1000);
  });

  it('should not navigate to hero 10', () => {
    cy.visit('http://localhost:4200/detail/10');
  });

  it('should delete hero 11', () => {
    cy.get('#heroes').click();
    cy.get('#11delete').click();
    cy.wait(1000);
  });

  it('should add new test hero', () => {
    cy.get('#add-hero-input').type('Test name');
    cy.get('#add-hero-button').click();
    cy.wait(1000);
  });

  it('should not add new test hero', () => {
    cy.get('#add-hero-input').clear();
    cy.get('#add-hero-button').click();
    cy.wait(1000);
  });
});
